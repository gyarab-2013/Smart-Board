Tomáš Krejča, Matouš Jokl

Anotace:
Cílem projektu Smart Board bylo využít mikrořadič zabudovaný v platformě Arduino novým způsobem.
Měl napodobit kdysi využívané PDA, tedy malé pomocníky a předchůdce smartfounů.
Využitelnost je testována na základních funkcí jako je malování, souborový adresář připojitelné SD karty nebo hra na motiv šachů.
K vizualizaci a ovládání byl použit dotykový displej.