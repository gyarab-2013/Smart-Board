﻿
#include <Adafruit_GFX.h>    // Core graphics library
#include <Adafruit_TFTLCD.h> // Hardware-specific library
#include <TouchScreen.h>

#define YP A1  // must be an analog pin, use "An" notation!
#define XM A2  // must be an analog pin, use "An" notation!
#define YM 7   // can be a digital pin
#define XP 6   // can be a digital pin

#define TS_MINX 150
#define TS_MINY 120
#define TS_MAXX 920
#define TS_MAXY 940

#define MINPRESSURE 5
#define MAXPRESSURE 1000

TouchScreen ts = TouchScreen(XP, YP, XM, YM, 300);

#define LCD_CS A3
#define LCD_CD A2
#define LCD_WR A1
#define LCD_RD A0
// optional
#define LCD_RESET A4

// ------------------------------------------------------------------------------------------------------------------------ BARVY
#define BLACK   0x0000
#define GREY    829999
#define BLUE    0x001F
#define RED     0xF800
#define GREEN   0x07E0
#define CYAN    0x07FF
#define MAGENTA 0xF81F
#define YELLOW  0xFFE0
#define WHITE   0xFFFF
#define Purple  0xF81A

#define color

Adafruit_TFTLCD tft(LCD_CS, LCD_CD, LCD_WR, LCD_RD, LCD_RESET);

#define BOXSIZE 40
#define PENRADIUS 3


// ------------------------------------------------------------------------------------------------------------------------ PAWN
class Pawn {
  public: // private:
    int x;
    int y;
    int id;
    uint16_t col; //colBlack = GREEN
    bool firstTurn = true;
    int isPlayer1; //boolean
    int wasHighlighted = 0;
    bool isAlive = true;

    Pawn (int _x, int _y, int _id, uint16_t _col, int _isPlayer1) {//boolean isPlayer1,
      x = _x;
      y = _y;
      id = _id;
      col = _col;
      isPlayer1 = _isPlayer1;
    }

    int getX() {
      return this -> x;
    }
    int getY() {
      return this -> y;
    }
    int getId() {
      return this -> id;
    }

    uint16_t getCol() {
      return this -> col;
    }
    bool getFirstTurn() {
      return this -> firstTurn;
    }
    int getPlayer() { //boolean
      return this -> isPlayer1;
    }
    int getHighlighted() {
      return this -> wasHighlighted;
    }
    bool getLive() {
      return this -> isAlive;
    }

    int setX(int newX) {
      x = newX;
    }
    int setY(int newY) {
      y = newY;
    }
    uint16_t setCol(uint16_t newCol) {
      col = newCol;
    }
    bool setFirstTurn(bool b) {
      firstTurn = b;
    }
    int setHighlighted(int h) {
      wasHighlighted = h;
    }
    bool setLive(int l) {
      isAlive = l;
    }
};
// ------------------------------------------------------------------------------------------------------------------------ BISHOP
class Bischop { //Bischop - střelec     //class Bischop: public Pesak
  public: // private:
    int x;
    int y;
    int id;
    uint16_t col; //colBlack = GREEN
    int wasHighlighted = 0;

  public:
    Bischop (int _x, int _y, int _id, uint16_t _col) {
      x = _x;
      y = _y;
      id = _id;
      col = _col;
    }

    int getX() {
      return this -> x;
    }
    int getY() {
      return this -> y;
    }
    int getId() {
      return this -> id;
    }
    uint16_t getCol() {
      return this -> col;
    }
    int getHighlighted() {
      return this -> wasHighlighted;
    }

    int setX(int newX) {
      x = newX;
    }
    int setY(int newY) {
      y = newY;
    }
    uint16_t setCol(uint16_t newCol) {
      col = newCol;
    }
    int setHighlighted(int h) {
      wasHighlighted = h;
    }
};
// ------------------------------------------------------------------------------------------------------------------------ ROOK
class Rook {
  public: // private:
    int x;
    int y;
    int id;
    uint16_t col; //colBlack = GREEN
    int wasHighlighted = 0;

  public:
    Rook (int _x, int _y, int _id, uint16_t _col) {
      x = _x;
      y = _y;
      id = _id;
      col = _col;
    }

    int getX() {
      return this -> x;
    }
    int getY() {
      return this -> y;
    }
    int getId() {
      return this -> id;
    }
    uint16_t getCol() {
      return this -> col;
    }
    int getHighlighted() {
      return this -> wasHighlighted;
    }

    int setX(int newX) {
      x = newX;
    }
    int setY(int newY) {
      y = newY;
    }
    uint16_t setCol(uint16_t newCol) {
      col = newCol;
    }
    int setHighlighted(int h) {
      wasHighlighted = h;
    }
};
void setup()
{
  Serial.begin(9600);
  uint16_t identifier = tft.readID(); //9325
  pinMode(10, OUTPUT);
  tft.begin(identifier);
  tft.setCursor(1, 1);
}

// ------------------------------------------------------------------------------------------------------------------------ ZAKLÁDÁNÍ PROMĚNÝCH
int pocitadlo = 0;

uint16_t col1 = BLUE;
uint16_t col2 = YELLOW;
boolean colb1 = false;
boolean colb2 = false;

uint16_t col3 = GREEN;
uint16_t col4 = WHITE;
boolean colb3 = false;
boolean colb4 = false;

uint16_t player1_col = WHITE;
uint16_t player2_col = BLACK;


boolean jeKresleni = true;
boolean prepinac = false;
boolean vypinac = false;

// šachy
boolean firstTime = true;
boolean isBlack = false;
boolean isWhite = false;

static int size_ = tft.width() / 8  ;
static int player2_startPosition = 6;
static int squaresInRow = 8;
static int signPosition = size_ / 5;
static int pawnsTotal = 16;
static int bishopsTotal = 4;
static int bounds = (tft.width() - size_) / size_;

//int X;
//int Y;
int X_clicked = -1;
int Y_clicked = -1;

int x_predch = -1;
int y_predch = -1;

int previous_P_index = -1;
int previous_B_index = -1;

int previous_X_clicked = -1;
int previous_Y_clicked = -1;

// id_s numerical values:
// 1  - 16 ... Pawns
// 17 - 20 ... Bishops
// 21 - 24 ... Rooks
// 25 - 26 ... Queens
// 27 - 30 ... Horses
// 31 - 32 ... Kings

Pawn pawn_1a(0, 1, 1, player1_col, 1);
Pawn pawn_2a(1, 1, 2, player1_col, 1); //int _x, int _y, int _id, uint16_t _col, int _isPlayer1
Pawn pawn_3a(2, 1, 3, player1_col, 1);
Pawn pawn_4a(3, 1, 4, player1_col, 1);
Pawn pawn_5a(4, 1, 5, player1_col, 1);
Pawn pawn_6a(5, 1, 6, player1_col, 1);
Pawn pawn_7a(6, 1, 7, player1_col, 1);
Pawn pawn_8a(7, 1, 8, player1_col, 1);
Pawn pawn_1b(0, player2_startPosition, 9, player2_col, 2);
Pawn pawn_2b(1, player2_startPosition, 10, player2_col, 2);
Pawn pawn_3b(2, player2_startPosition, 11, player2_col, 2);
Pawn pawn_4b(3, player2_startPosition, 12, player2_col, 2);
Pawn pawn_5b(4, player2_startPosition, 13, player2_col, 2);
Pawn pawn_6b(5, player2_startPosition, 14, player2_col, 2);
Pawn pawn_7b(6, player2_startPosition, 15, player2_col, 2);
Pawn pawn_8b(7, player2_startPosition, 16, player2_col, 2);
Pawn pawnField [] = {pawn_1a, pawn_2a, pawn_3a, pawn_4a, pawn_5a, pawn_6a, pawn_7a, pawn_8a, pawn_1b, pawn_2b, pawn_3b, pawn_4b, pawn_5b, pawn_6b, pawn_7b, pawn_8b};

Bischop b1a(2, 0, 17, WHITE);
Bischop b2a(5, 0, 18, WHITE);
Bischop b1b(2, player2_startPosition + 1, 19, BLACK);
Bischop b2b(5, player2_startPosition + 1, 20, BLACK);
Bischop bishopField [] = {b1a, b2a, b1b, b2b};

Rook r1a(0, 0, 21, WHITE);
Rook r2a(7, 0, 22, WHITE);
Rook r1b(0, player2_startPosition + 1, 23, BLACK);
Rook r2b(7, player2_startPosition + 1, 24, BLACK);
Rook rookField [] = {r1a, r2a, r1b, r2b};

//Figure player1Field [] = {pawnField, bishopField};
//Pawn player1Field [] = {pawnField};

bool player1IsOnTurn = true;
bool player1Win = false;
bool player2Win = false;

static double zaokrouhli(double c) {
  double o = c;
  while (c > 1) {
    c = c - 1;
  }
  if (c < 0.45) {
    o = o - c;
  } else {
    o = o + 1 - c;
  }
  return o;
}
int prepocti(int s) {
  int s_ = s;
  while (s_ > size_) {
    s_ = s_ - size_;
  }
  return s - s_;
}

void setSign(int x, int y) {
  tft.setCursor(x + signPosition, y + signPosition);
  tft.setTextSize(2);
  tft.setTextColor(RED);
}

void drawPawnSign() {
  tft.print("P");
}
void drawPawns() {
  for (int i = 0; i < pawnsTotal ; i++) {
    int x = pawnField[i].getX()  * size_;
    int y = pawnField[i].getY()  * size_;
    //    int id = pawnField[i].getId();
    uint16_t col_pawn = pawnField[i].getCol();
    if (pawnField[i].getLive()) {
      tft.fillRect(x, y , size_ , size_, col_pawn);
      setSign(x, y);
      tft.print("P");
    }
  }
}

void drawBishopSign() {
  tft.print("B");
}
void drawBishops() {
  for (int i = 0; i < 4; i++) {
    int x =  bishopField[i].getX() * size_;
    int y = bishopField[i].getY() * size_;
    uint16_t col = bishopField[i].getCol();
    tft.fillRect(x, y, size_ , size_, col);
    setSign(x, y);
    tft.print("B");
  }
}

void drawRookSign() {
  tft.print("R");
}
void drawRooks() {
  for (int i = 0; i < 4; i++) {
    int x =  rookField[i].getX() * size_;
    int y = rookField[i].getY() * size_;
    uint16_t col = rookField[i].getCol();
    tft.fillRect(x, y, size_ , size_, col);
    setSign(x, y);
    tft.print("R");
  }
}


int getPawnPositionInArray() {
  for (int i = 0; i < pawnsTotal; i++) {
    if (pawnField[i].getX()  == X_clicked && pawnField[i].getY() == Y_clicked ) {
      return i;
    }
  }
  return -1;
}
int getBischopPositionInArray() {
  for (int i = 0; i < 4; i++) {
    if ( bishopField[i].getX() == X_clicked && bishopField[i].getY() == Y_clicked ) {
      return i;
    }
  }
  return -1;
}
int getRookPositionInArray() {
  for (int i = 0; i < 4; i++) {
    if ( rookField[i].getX() == X_clicked && rookField[i].getY() == Y_clicked ) {
      return i;
    }
  }
  return -1;
}

void setPawnColor(int IndexInArray) {
  if (pawnField[IndexInArray].getCol() == WHITE) {
    isWhite = true;
  }
  if (pawnField[IndexInArray].getCol() == GREY) {
    isBlack = true;
  }
}


// ------------------------------------------------------------------------------------------------------------------------ OZNAČENÍ POLÍČKA
void drawChooseSquare(int IndexInArray) {
  //if (pawnField[IndexInArray].getX() != X_clicked && pawnField[IndexInArray].getY() != Y_clicked) {

  if (X_clicked == x_predch && Y_clicked == y_predch && prepinac) {

    if ((X_clicked * size_ % 2 == 0 && Y_clicked * size_ % 2 != 0) || (X_clicked * size_ % 2 != 0 && Y_clicked * size_ % 2 == 0 )) { //
      colb1 = true;         //určí barvu políčku
      //Serial.println("colb1");
    } else {
      colb2 = true;
      //Serial.println("colb2");
    }

    if (colb1) {
      tft.fillRect(X_clicked * size_, Y_clicked * size_, size_ , size_, col1);
    }
    if (colb2) {
      tft.drawRect(X_clicked * size_, Y_clicked * size_, size_ , size_, col2);
    }
    colb1 = false;
    colb2 = false;

  } else {
    if (Y_clicked < tft.width() / size_) {
      tft.fillRect(X_clicked * size_, Y_clicked * size_, size_ , size_, Purple); //Purple - barva označení políčka
    }
  }
  //}
  x_predch = X_clicked;
  y_predch = Y_clicked;
  prepinac = !prepinac;
  tft.fillRect(0, tft.width(), tft.width(), tft.width() / 3, BLACK); //překreslení spodní části obrazovky
}

bool isPawnMoveAvaliable(bool isPlayer1OnTurn, int IndexInArray, int x, int y) { //IndexInArray ... previous_P_index
  int x_pawn = pawnField[IndexInArray].getX();
  int y_pawn = pawnField[IndexInArray].getY();

  if (isPlayer1OnTurn) {
    if (vyhodnotPolicko(isPlayer1OnTurn, IndexInArray, x, y) == 0 && x == x_pawn) { //0 -je prázdné
      if ( (pawnField[IndexInArray].getFirstTurn() && y == y_pawn + 2) || y == y_pawn + 1) { //&& vyhodnotPolicko(isPlayer1OnTurn, IndexInArray, x, y - 1) == 0
        return true;
      }
    }
    if (vyhodnotPolicko(isPlayer1OnTurn, IndexInArray,  x, y) == 1 && (x == x_pawn + 1 || x == x_pawn - 1) ) { //1 -je tam nepřítel
      //vymazat figurku na indexu
      return true;
    }
    return false;

  } else {
    if (vyhodnotPolicko(isPlayer1OnTurn, IndexInArray, x, y) == 0 && x == x_pawn) { //0 -je prázdné
      if ( (pawnField[IndexInArray].getFirstTurn() && y == y_pawn - 2 ) || y == y_pawn - 1) { //&& vyhodnotPolicko(isPlayer1OnTurn, IndexInArray, x, y + 1) == 0
        return true;
      }
    }
    if (vyhodnotPolicko(isPlayer1OnTurn, IndexInArray, x, y) == 1 && (x == x_pawn + 1 || x == x_pawn - 1) ) { //1 -je tam nepřítel
      //vymazat figurku na indexu
      return true;
    }
    return false;
  }
}
bool isBishopMoveCorrect(int x, int y, int IndexInArray ) { //bool isPlayer1OnTurn,
  //tft.width()
  Serial.println("fukin function");

  int bX  = bishopField[IndexInArray].getX();
  int bY  = bishopField[IndexInArray].getY();

  Serial.println("x: " + (String)x); Serial.println("y: " + (String)y); Serial.println("bX: " + (String)bX); Serial.println("bY: " + (String)bY);

  if (x > bX ) {
    if (y > bY) {
      while (x != bX && y != bY) { // ||
        x--; y--;
        if (x == -1 || y == - 1) {
          Serial.println("break 1");
          break;
        }
        Serial.println("1. while...");
      }
    } else {
      while (x != bX || y != bY) {
        x--; y++;
        if (x == -1 || y == 10) {
          Serial.println("break 2");
          break;
        }
        Serial.println("2. while...");
      }
    }
  } else {
    if ( y < bY) {
      while (x != bX || y != bY) {
        x++; y++;
        //        if (x != bX || y != bY) {
        //          Serial.println("break ***");
        //          break;
        //        }
        if (x == 10 || y == 10) {
          Serial.println("break 3");
          break;
        }
        Serial.println("3. while...");
      }
    } else {
      while (x != bX || y != bY) {
        x++; y--;
        if (x == 10 || y == - 4) {
          Serial.println("break 4");
          break;
        }
        Serial.println("4. while...");
      }
    }
  }
  Serial.println("x: " + (String)x);
  Serial.println("y: " + (String)y);
  Serial.println("bX: " + (String)bX);
  Serial.println("bY: " + (String)bY); Serial.println("");

  if (x == bX && y == bY) {
    Serial.println("return: TRUE");
    return true;
  }
  Serial.println("return: FALSE");
  return false;
}

int vyhodnotPolicko(bool isPlayer1OnTurn, int index, int x, int y) { //int EnemyCol,
  for (int i = 0; i < sizeof(pawnField); i++) {
    if ( x == pawnField[i].getX() && y == pawnField[i].getY() ) {
      if (isPlayer1OnTurn != pawnField[i].getPlayer()) { //původně....   EnemyCol == pawnField[i].getCol()
        Serial.print("....WOOOW!");
        kill(i, 0); // 0 -pawn        //původně...index -ale špatně
        return 1; //je tam nepřítel
      } else {
        return 2; //je tam moje figurka
      }
    }
  }
  for (int i = 0; i < sizeof(bishopField); i++) {
    if ( x == bishopField[i].getX() && y == bishopField[i].getY() ) {
      if (true) { // EnemyCol == bishopField[i].getCol()
        kill(index, 1);
        return 1;
      } else {
        return 2;
      }
    }
  }
  return 0; //políčko je prázdné
}
void kill(int index, int c) {

  if (c == 0) {
    pawnField[index].setLive(false);
    Pawn p (index, 9, -1, Purple, 1);
    pawnField[index] = p; //int _x, int _y, int _id, uint16_t _col, int _isPlayer1
    //Pawn p(5, 6, -1, GREEN, false, 1, 1);
    //pawnField[index] = p;
    drawPawns();
  }
  if (c == 1) {
    for (int i = 1; i < sizeof(bishopField) - 1; i++) {
      bishopField[index + i] = bishopField[index + i - 1];
    }
  }
}

int showPawnPossibilities(int x, int y, int IndexInArray) { //vykreslí kam figurka může       //dríve:  , boolean firstTime
  int xPawn = pawnField[IndexInArray].getX() ;
  int yPawn = pawnField[IndexInArray].getY() ;
  int id = pawnField[IndexInArray].getId();

  if (y < 7) {
    if (id <= pawnsTotal / 2) { //IndexInArray < pawnsTotal / 2
      tft.fillRect(x * size_, (y * size_) + size_, size_ , size_, RED);
    }
    if (id > pawnsTotal / 2) { //IndexInArray >= pawnsTotal / 2
      tft.fillRect(x * size_, (y * size_) - size_, size_ , size_, RED);
    }
  }
  drawChooseSquare(IndexInArray);
}

void showBishopPossibilities(int IndexInArray) {
  //tft.width()
  int bX = bishopField[IndexInArray].getX();
  int bY = bishopField[IndexInArray].getY();

  while ( bX  <=  7) { // && x + 1 < polePesaku[i].getX() && y + 1 < polePesaku[i].getY()
    bX++;
    bY++;
    tft.fillRect(bX * size_, bY * size_, size_ , size_, RED);
  }
  bX = bishopField[IndexInArray].getX(); bY = bishopField[IndexInArray].getY();

  while ( bX > 0 ) { // && x - 1 < polePesaku[i].getX() && y + 1 < polePesaku[i].getY()
    bX--;
    bY++;
    tft.fillRect(bX * size_, bY * size_, size_ , size_, RED);
  }
  bX = bishopField[IndexInArray].getX(); bY = bishopField[IndexInArray].getY();

  while ( bX > 0 ) {
    bX--;
    bY--;
    tft.fillRect(bX * size_, bY * size_, size_ , size_, RED);
  }
  bX = bishopField[IndexInArray].getX(); bY = bishopField[IndexInArray].getY();

  while ( bX <= 7) {
    bX++;
    bY--;
    tft.fillRect(bX * size_, bY * size_, size_ , size_, RED);
  }
}
void showRookPosibilities(int IndexInArray) {
  int rX = rookField[IndexInArray].getX();
  int rY = rookField[IndexInArray].getY();

  while (rX < 7) {
    rX++;
    tft.fillRect(rX * size_, rY * size_, size_ , size_, RED);
  }
  rX = rookField[IndexInArray].getX();

  while (rX > 0) {
    rX--;
    tft.fillRect(rX * size_, rY * size_, size_ , size_, RED);
  }
  rX = rookField[IndexInArray].getX();

  while (rY < 7) {
    rY++;
    tft.fillRect(rX * size_, rY * size_, size_ , size_, RED);
  }
  rY = rookField[IndexInArray].getY();

  while (rY > 0) {
    rY--;
    tft.fillRect(rX * size_, rY * size_, size_ , size_, RED);
  }
}

void setPawnXY(int x, int y, int IndexInArray, boolean firstTurn) {
  pawnField[IndexInArray].setX(x);
  pawnField[IndexInArray].setY(y);
  pawnField[IndexInArray].setFirstTurn(false);
}
void setBishopXY(int x, int y, int IndexInArray) {
  bishopField[IndexInArray].setX(x);
  bishopField[IndexInArray].setY(y);
}

// ------------------------------------------------------------------------------------------------------------------------ ŠACHOVNICE
void drawChessBoard() {
  static int posun;
  static int size_ = tft.width() / squaresInRow; //squaresInRow = 8

  for (int j = 0; j < squaresInRow; j++) {
    for (int i = 0; i < size_; i++) {
      if ( (i % 2 == 0 && j % 2 != 0) || (i % 2 != 0 && j % 2 == 0 )) {
        tft.fillRect(i * size_, j * size_ , size_ , size_, col1); //blue
      } else {
        //zlutePole[i] = i; zlutePole[i + 1] = j;
        tft.fillRect(i * size_, j * size_ , size_ , size_, col2); //yellow
      }
    }
  }
  firstTime = false;
}

// ------------------------------------------------------------------------------------------------------------------------ LOOP
void loop()
{
  digitalWrite(13, HIGH);
  TSPoint p = ts.getPoint();
  digitalWrite(13, LOW);

  pinMode(XM, OUTPUT);
  pinMode(YP, OUTPUT);
  p.x = map(p.x, TS_MINX, TS_MAXX, tft.width(), 0) - 2;
  p.y = map(p.y, TS_MINY, TS_MAXY, tft.height() - BOXSIZE * 2, 0) - 6;

  if (p.z < MAXPRESSURE && p.z > MINPRESSURE - 5)
  {

    X_clicked = prepocti(p.x) / size_; //Serial.println("X_clicked - in loop"); Serial.println(X_clicked);
    Y_clicked = prepocti(p.y) / size_; //Serial.println("Y_clicked - in loop"); Serial.println(Y_clicked);

    //    drawChooseSquare(index);

    int pawnIndex = getPawnPositionInArray();
    int bishopIndex = getBischopPositionInArray();
    int rookIndex = getRookPositionInArray();

    int X_atPawnIndex = pawnField[pawnIndex].getX();
    int Y_atPawnIndex = pawnField[pawnIndex].getY();
    int X_atBischopIndex = bishopField[bishopIndex].getX();
    int Y_atBischopIndex = bishopField[bishopIndex].getY();

    int pawnColor = pawnField[pawnIndex].getCol();
    uint16_t bishopColor = bishopField[bishopIndex].getCol();
    uint16_t rookColor = rookField[rookIndex].getCol();

    boolean firstTurn = pawnField[pawnIndex].getFirstTurn();
    //boolean isPlayer1;
    //if (pawnIndex != -1) isPlayer1 = pawnField[pawnIndex].getPlayer();;
    boolean isPlayer1 = pawnField[pawnIndex].getPlayer();
    //int bounds = (tft.width() - size_) / size_;

    drawChessBoard();
    drawPawns();
    drawBishops();
    drawRooks();

    if (!firstTime) {
      drawChooseSquare(pawnIndex);
      //Serial.println("previous_index"); Serial.println(previous_index);
      //Serial.println("pawnIndex"); Serial.println(pawnIndex); Serial.println("");
      Serial.println("bishopIndex"); Serial.println(bishopIndex);
      Serial.println("previous_B_index"); Serial.println(previous_B_index);
      Serial.println("");

      //>>_PAWNS_<<
      if (pawnField[previous_P_index].getHighlighted() == 1  ) { //&& pawnIndex == -1
        if (player1IsOnTurn && pawnField[previous_P_index].getId() <= pawnsTotal / 2) {
          if (isPawnMoveAvaliable(player1IsOnTurn, previous_P_index, X_clicked, Y_clicked)) {
            //PŘIDAT ČÍSLO HRÁČE!!!
            setPawnXY(X_clicked, Y_clicked, previous_P_index, firstTurn);
            //previous_X_clicked, previous_Y_clicked
            player1IsOnTurn = !player1IsOnTurn;
          }
        }
        if (!player1IsOnTurn && pawnField[previous_P_index].getId() > pawnsTotal / 2) {
          if (isPawnMoveAvaliable(player1IsOnTurn, previous_P_index, X_clicked, Y_clicked)) {
            setPawnXY(X_clicked, Y_clicked, previous_P_index, firstTurn);
            player1IsOnTurn = !player1IsOnTurn;
          }
        }
        pawnField[previous_P_index].setHighlighted(0);
      }
      if ( pawnIndex != -1 && pawnField[pawnIndex].getLive()) {
        showPawnPossibilities(X_clicked, Y_clicked, pawnIndex); //pawnIndex, firstTurn
        pawnField[pawnIndex].setHighlighted(1);
        previous_P_index = pawnIndex;
      }

      //>>_BISHOPS_<<
      if (bishopField[previous_B_index].getHighlighted() == 1) {
        if (player1IsOnTurn && (bishopField[previous_B_index].getId() == 17 || bishopField[previous_B_index].getId() == 18)) {
          if (isBishopMoveCorrect(X_clicked, Y_clicked, previous_B_index)) {
            Serial.println("is CORRECT!!!");
            setBishopXY( X_clicked, Y_clicked, previous_B_index);
            player1IsOnTurn = !player1IsOnTurn;
          }
        }
        if (!player1IsOnTurn && (bishopField[previous_B_index].getId() == 19 || bishopField[previous_B_index].getId() == 20)) {
          if (isBishopMoveCorrect(X_clicked, Y_clicked, previous_B_index)) {
            Serial.println("is CORRECT!!!");
            setBishopXY( X_clicked, Y_clicked, previous_B_index);
            player1IsOnTurn = !player1IsOnTurn;
          }
        }
        bishopField[previous_B_index].setHighlighted(0);
      }
      if ( bishopIndex != -1 ) { //bounds = 7
        showBishopPossibilities(bishopIndex);
        bishopField[bishopIndex].setHighlighted(1);
        previous_B_index = bishopIndex;
      }

      //Rooks
      if (rookIndex != -1) {
        showRookPosibilities(rookIndex);
      }

    }//firstTime <--- condition

    if (Y_atPawnIndex * size_ == tft.width() - size_) { // pesak na konci sachovnice je zmenen na zelenou
      pawnField[pawnIndex].setCol(GREEN);
      tft.fillRect(X_atPawnIndex * size_, Y_atPawnIndex * size_, size_ , size_, pawnField[pawnIndex].getCol());
    }
    if (Y_atPawnIndex == 0) { // pesak na konci sachovnice je zmenen na CYAN
      pawnField[pawnIndex].setCol(CYAN);
      tft.fillRect(X_atPawnIndex * size_, Y_atPawnIndex * size_, size_ , size_, pawnField[pawnIndex].getCol());
    }

    //      drawChessBoard();
    //      drawPawns();
    //      drawBishops();

    if (pawnIndex > -1 || bishopIndex > -1 || rookIndex > -1) {
      tft.setCursor(10, 10 + tft.width());
      tft.setTextSize(2.0);
      tft.setTextColor(RED);

      if (pawnIndex > -1) {
        tft.print("Pesak number: ");
        tft.print(pawnIndex);
        tft.setCursor(10, 30 + tft.width());
        tft.print("Color: ");
        if (pawnColor == BLACK) tft.print("Black");
        if (pawnColor == WHITE) tft.print("White");
      }
      if (bishopIndex > -1) {
        tft.print("Bishop number: ");
        tft.print(bishopIndex);
        tft.setCursor(10, 30 + tft.width());
        tft.print("Color: ");
        if (bishopColor == BLACK) tft.print("Black");
        if (bishopColor == WHITE) tft.print("White");
      }
      if (rookIndex > -1) {
        tft.print("Rook number: ");
        tft.print(rookIndex);
      }
      tft.setCursor(10, 50 + tft.width());
      tft.print("IsOnTurn: ");
      tft.print(player1IsOnTurn == true);

      // if (isBlack == true) tft.print("Black"); isBlack = false;
      // if (isWhite == true) tft.print("White"); isWhite = false;
      //  }
    }
  }
}
