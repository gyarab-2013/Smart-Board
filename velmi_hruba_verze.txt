#include <Adafruit_GFX.h>    // Core graphics library
#include <Adafruit_TFTLCD.h> // Hardware-specific library
#include <TouchScreen.h>


#define YP A1  // must be an analog pin, use "An" notation!
#define XM A2  // must be an analog pin, use "An" notation!
#define YM 7   // can be a digital pin
#define XP 6   // can be a digital pin

#define TS_MINX 150
#define TS_MINY 120
#define TS_MAXX 920
#define TS_MAXY 940

#define MINPRESSURE 5
#define MAXPRESSURE 1000

TouchScreen ts = TouchScreen(XP, YP, XM, YM, 300);


#define LCD_CS A3
#define LCD_CD A2
#define LCD_WR A1
#define LCD_RD A0
// optional
#define LCD_RESET A4

#define	BLACK   0x0000
#define	BLUE    0x001F
#define	RED     0xF800
#define	GREEN   0x07E0
#define CYAN    0x07FF
#define MAGENTA 0xF81F
#define YELLOW  0xFFE0
#define WHITE   0xFFFF

Adafruit_TFTLCD tft(LCD_CS, LCD_CD, LCD_WR, LCD_RD, LCD_RESET);


#define BOXSIZE 40
#define PENRADIUS 3


void setup()
{
  Serial.begin(9600);
  uint16_t identifier = tft.readID(); //9325
  pinMode(10, OUTPUT);
  tft.begin(identifier);
  tft.setCursor(1, 1);
}


//int bufferArray[150][150];
uint16_t col1 = BLUE;
uint16_t col2 = RED;
uint16_t col3 = GREEN;
uint16_t col4 = WHITE;
int pocitadlo = 0;
boolean jeKresleni = true;
boolean prepinac = true;
int x;
int y;
int x0;
int y0;
//int[] pole = new int[2];

static double zaokrouhli(double c) {
  double o = c;
  while (c > 1) {
    c = c - 1;
  }
  if (c < 0.45) {
    o = o - c;
  } else {
    o = o + 1 - c;
  }
  return o;
}

void hvezdicka(TSPoint p) {
  if (prepinac) {
    x = p.x;
    y = p.y;
    tft.setCursor(x - 10, y - 16);
    tft.print("*");
    //prepinac = !prepinac;
  }
}
void kolecko(TSPoint p) {
  if (prepinac) {
    x = p.x;
    y = p.y;
    tft.setCursor(x - 10, y - 16);
    tft.print("o");
    //prepinac = !prepinac;
  }
}

void loop()
{
  digitalWrite(13, HIGH);
  TSPoint p = ts.getPoint();
  digitalWrite(13, LOW);

  pinMode(XM, OUTPUT);
  pinMode(YP, OUTPUT);
  p.x = map(p.x, TS_MINX, TS_MAXX, tft.width(), 0) - 2;
  p.y = map(p.y, TS_MINY, TS_MAXY, tft.height() - BOXSIZE * 2, 0) - 6;
  tft.setCursor(30, 30); //240x320
  tft.setTextSize(2);

  tft.setCursor(p.x - 5, p.y - 20);
  tft.fillRect(10, 10, 20, 20, GREEN); //x, y, velikost x, velikost y, barva
  tft.fillRect(10, 30, 20, 20, RED);
  tft.fillRect(10, 50, 20, 20, BLUE);
  tft.fillRect(10, 70, 20, 20, WHITE);

  if (p.z < MAXPRESSURE && p.z > MINPRESSURE - 5)
  {

    //zkus(p);
    zjistiBod(p);
    //cara();
    //caraTest();
    kruznice();
    //kolecko(p);

    //zkusObdelnik();
    //najdiSouradnice();

    Serial.println("p.x");
    Serial.println(p.x);
    Serial.println("p.y");
    Serial.println(p.y);


  }
}

void kruznice() {
  int i = 0;
  int xx = 62;
  int yy = 62;
 
  while (xx > x0) {
    if (i % 2 == 0) {
      xx = xx - i;
      tft.setCursor(xx, yy);
      tft.print(".");
    } else {
      yy = yy - i;
      tft.setCursor(xx, yy);
      tft.print(".");
    }
    i++;
  }

}


void cara() {
  (double)x, y;
  if (x > y) {
    int m = (x - x0) / (y - y0);
    while (x > x0) {
      tft.fillRect(x0, y0, 5, 5, RED);
      x0 = x0 + 1;
      y0 = y0 + (int)zaokrouhli(m);
    }
  } else {
    int m = (y - y0) / (x - x0);
    while (y > y0) {
      tft.fillRect(x0, y0, 5, 5, RED);
      y0 = y0 + 1;
      x0 = x0 + (int)zaokrouhli(m);
    }
  }
}
void caraTest() {
  //(double)x, y;
  x0 = 0;
  y0 = 0;
  x = 100;
  y = 100;
  cara();
}

void zkus(TSPoint p) {
  if (prepinac) {
    //prvniBod
    x0 = p.x;
    y0 = p.y;
    tft.fillRect(10, 10, 2, 2, GREEN);
  } else {
    //druhyBod
    x = p.x;
    y = p.y;
    tft.fillRect(100, 100, 2, 2, RED);
  }
  if (x0 != x && y0 != y) {
    prepinac = ! prepinac;
  }
}
void zjistiBod(TSPoint p) {
  if (prepinac) {
    //prvniBod
    x0 = p.x;
    y0 = p.y;
    tft.fillRect(x0, y0, 2, 2, GREEN);
  } else {
    //druhyBod
    x = p.x;
    y = p.y;
    tft.fillRect(x, y, 2, 2, RED);

  }
  if (x0 != x && y0 != y ) { //|| (x - x0 > 5 && y - y0 > 5 || x0 - x > 5 && y0 - y > 5)
    prepinac = ! prepinac;
  }
}

void zkusObdelnik() {
  //tft.fillRect(0, 100, 150, 150, GREEN);
  tft.fillRect(0, 100, 200, 20, BLUE);
}
